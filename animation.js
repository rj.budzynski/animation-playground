const { sin, cos, PI, sqrt, random } = Math;
const TWO_PI = 2 * PI;

export class SimpleAnimation {
    // canvas mode could be '2d', 'webgl' or 'webgl2'
    constructor(canvas, mode = '2d', options = {}) {
        this.running = false;
        this.lastRedraw = undefined;
        this.gc = canvas.getContext(mode, options);
    }

    init() {
        return this.setSize();
    }

    setSize(width, height) {
        const canvas = this.gc.canvas;
        this.width = width || canvas.clientWidth;
        this.height = height || canvas.clientHeight;
        return this;
    }

    get height() {
        return this.gc.canvas.height;
    }

    set height(h) {
        this.gc.canvas.height = h;
    }

    get width() {
        return this.gc.canvas.width;
    }

    set width(w) {
        this.gc.canvas.width = w;
    }

    nextPos(ms) {
        if (this.lastRedraw === undefined) {
            this.phi = random() * TWO_PI;
        } else {
            const dt = ms - this.lastRedraw;
            // the below would be motion at constant angular velocity
            // this.phi += (5e-4 * dt);
            // instead, use energy conservation under
            // a uniform gravitational pull
            const omega =
                2e-4 * sqrt(1 + ((3 * this.width) / 8) * (1 - sin(this.phi)));
            this.phi += dt * omega;
            this.phi %= TWO_PI;
        }
    }

    nextFrame(ms) {
        // draw the next frame according to elapsed milliseconds
        const { gc, width, height } = this;
        this.nextPos(ms);
        gc.lineWidth = 2;
        gc.clearRect(0, 0, width, height);
        gc.beginPath();
        gc.strokeStyle = 'rgba(0, 255, 255, 1)';
        gc.arc(width / 2, height / 2, (3 * width) / 8, 0, TWO_PI);
        gc.stroke();
        gc.beginPath();
        gc.fillStyle = 'rgba(255, 255, 0, 1)';
        gc.arc(
            (width / 2) * (1 + 0.75 * cos(this.phi)),
            height / 2 - ((3 * width) / 8) * sin(this.phi),
            7,
            0,
            TWO_PI
        );
        gc.fill();
        return ms;
    }

    run(ms) {
        if (!this.running) return;
        this.lastRedraw = this.nextFrame(ms);
        this.running = requestAnimationFrame(ms => this.run(ms));
    }

    start() {
        this.run((this.running = performance.now()));
    }

    stop() {
        this.running = false;
        this.lastRedraw = undefined;
        this.gc.canvas.style = null; // reset to default style if altered
    }

    pause() {
        this.running = false;
    }

    resume() {
        this.lastRedraw = performance.now();
        this.run((this.running = this.lastRedraw));
    }
}

// OPTIONAL MIXIN: FULLSCREEN MODE ENABLER

export function enableFullScreen(toggleStyle = {}) {
    if (document.fullscreenEnabled) {
        const fsToggle = (this.fsToggle = document.createElement('p'));
        Object.assign(
            fsToggle.style,
            {
                position: 'absolute',
                right: '1em',
                top: '0',
                margin: '0',
                fontWeight: 'bolder',
                fontSize: 'x-large',
                color: 'gold',
                cursor: 'pointer'
            },
            toggleStyle
        );
        fsToggle.title = 'toggle fullscreen';
        fsToggle.textContent = '⛶';
        this.gc.canvas.after(fsToggle);
        fsToggle.addEventListener('click', ev => {
            this.toggleFullscreen();
            ev.stopPropagation();
        });
        document.addEventListener('fullscreenchange', () => this.setSize());
        this.__proto__.toggleFullscreen = toggleFullscreen;
    }
}

function toggleFullscreen() {
    if (!document.fullscreenEnabled) {
        alert('fullscreen mode not available!');
        return;
    }
    const mainEl = document.querySelector('main');
    if (!document.fullscreenElement) mainEl.requestFullscreen();
    else document.exitFullscreen();
}

// OPTIONAL MIXIN: OSD WITH FPS DISPLAY AND POTENTIALLY OTHER STUFF

export function enableOSD(osdStyle = {}) {
    this.osd = document.createElement('p');
    this.fpsNode = document.createElement('span');
    this.osd.appendChild(this.fpsNode);
    Object.assign(
        this.osd.style,
        {
            position: 'absolute',
            left: '1em',
            top: '0',
            fontWeight: 'bold',
            color: 'white'
        },
        osdStyle
    );
    this.gc.canvas.after(this.osd);
    this.__proto__.updateOSD = updateOSD;
    this.nextFrame = ms => {
        const t = this.__proto__.nextFrame.call(this, ms);
        this.updateOSD.call(this, ms);
        return t;
    };
    
    this.pause = () => {
        this.__proto__.pause.call(this);
        this.fpsNode.textContent = '0 fps';
    };
}

function updateOSD(ms) {
    if (this.lastUpdate === undefined) {
        this.lastUpdate = ms;
        this.frames = 0;
        return;
    }
    const dt = ms - this.lastUpdate;
    ++this.frames;
    if (dt > 1e3) {
        this.fpsNode.textContent = `${(
            (this.frames / dt) *
            1e3
        ).toFixed()} fps`;
        this.lastUpdate = ms;
        this.frames = 0;
    }
}
