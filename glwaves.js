import { GlNoise } from './glnoise.js';
import { enableFullScreen, enableOSD } from './animation.js';
import { shaderPair } from './shaders/glwaves-shaders.js';

export class GlWaves extends GlNoise {
    constructor(canvas) {
        super(canvas);
        this.shaders = shaderPair;
    }

    init() {
        const gc = this.gc;
        super.init().setUniform('2f', 'resolution', this.width, this.height);
        gc.canvas.parentElement.style.backgroundColor = 'rgb(0, 217, 255)';

        enableFullScreen.call(this);
        enableOSD.call(this);

        return this;
    }

    get time() {
        return this._time;
    }

    set time(t) {
        this.setUniform('1f', 'time', t);
        this._time = t;
    }

    nextFrame(ms) {
        const { gc, lastRedraw } = this;
        if (lastRedraw === undefined) {
            this.time = -Math.random() * 1e6;
        } else {
            const dt = ms - lastRedraw;
            this.time += dt;
        }
        gc.drawArrays(gc.TRIANGLES, 0, 6);
        return ms;
    }
}
