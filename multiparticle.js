import { WebGL2SimpleAnimation } from './gltools.js';
import { shaderPair } from './shaders/multiparticle-shaders.js';

const { min, random, PI, sin, cos, sqrt } = Math;
const TWO_PI = PI * 2;

export class GlMultiparticle extends WebGL2SimpleAnimation {
    init({
        color = [1, 1, 0.2],
        radius = 0.0075,
        accel = 3e-7,
        nParticles = 999
    } = {}) {
        const { gc, width, height } = super.init();
        Object.assign(this, { radius, accel, nParticles });

        gc.viewport(0, 0, width, height);
        gc.clearColor(0, 0, 0, 0);

        gc.useProgram(
            this.createProgramFromSources(gc, shaderPair, {}, [
                'v_position',
                'v_velocity',
                'v_yenergy'
            ])
        );

        // set fixed uniforms

        this.setUniform('1f', 'radius', radius);
        this.setUniform('1f', 'accel', accel);
        this.setUniform(
            '1f',
            'u_radius',
            1.1 * radius * min(width, height) // beware this won't work right on a non-square
        );
        this.setUniform('3fv', 'color', color);

        this.initBuffers();

        return this;
    }

    createInitialConditions() {
        // override this for different effects
        // must return a Float32Array of length 5*nParticles
        // with [x, y, vx, vy, yenergy, ...]
        const vertexAttribData = new Float32Array(5 * this.nParticles);
        for (let i = 0; i < this.nParticles; ++i) {
            let phi = random() * PI,
                r = 0.1 * sqrt(random());
            vertexAttribData[5 * i] = 0.5 + cos(phi) * r;
            vertexAttribData[5 * i + 1] = this.radius + sin(phi) * r;
            phi = random() * TWO_PI;
            r = 5e-4 * (Math.exp(random()) - 1);
            vertexAttribData[5 * i + 2] = r * cos(phi);
            vertexAttribData[5 * i + 3] = r * sin(phi);
            vertexAttribData[5 * i + 4] =
                0.5 * vertexAttribData[5 * i + 3] ** 2 +
                this.accel * vertexAttribData[5 * i + 1];
        }
        return vertexAttribData;
    }

    initBuffers() {
        const gc = this.gc;
        this.vaos = [gc.createVertexArray(), gc.createVertexArray()];
        this.buffers = [gc.createBuffer(), gc.createBuffer()];
        for (const i of [0, 1]) {
            gc.bindVertexArray(this.vaos[i]);
            gc.enableVertexAttribArray(0);
            gc.enableVertexAttribArray(1);
            gc.enableVertexAttribArray(2);
            gc.bindBuffer(gc.ARRAY_BUFFER, this.buffers[i]);
            gc.vertexAttribPointer(
                0, // this.i_positionPtr,
                2,
                gc.FLOAT,
                false,
                20,
                0
            );
            gc.vertexAttribPointer(
                1, // this.i_velocityPtr,
                2,
                gc.FLOAT,
                false,
                20,
                8
            );
            gc.vertexAttribPointer(
                2, // yenergy
                1,
                gc.FLOAT,
                false,
                20,
                16
            );
            gc.bindVertexArray(null);
        }
    }

    bindBuffers() {
        const [readBuffer, writeBuffer] = this.buffers;
        const gc = this.gc;

        gc.bindBufferBase(gc.TRANSFORM_FEEDBACK_BUFFER, 0, writeBuffer);
        gc.bindBuffer(gc.ARRAY_BUFFER, readBuffer);
        gc.bindVertexArray(this.vaos[0]);
    }

    swapBuffers() {
        this.buffers.reverse();
        this.vaos.reverse();
    }

    nextFrame(ms) {
        const { lastRedraw, gc } = this;

        if (lastRedraw === undefined) {
            const vertexAttribData = this.createInitialConditions();
            const [readBuffer, writeBuffer] = this.buffers;
            gc.bindBuffer(gc.ARRAY_BUFFER, readBuffer);
            gc.bufferData(gc.ARRAY_BUFFER, vertexAttribData, gc.DYNAMIC_COPY);
            gc.bindBuffer(gc.ARRAY_BUFFER, writeBuffer);
            gc.bufferData(
                gc.ARRAY_BUFFER,
                new Float32Array(5 * this.nParticles),
                gc.DYNAMIC_COPY
            );
        } else {
            const dt = ms - lastRedraw;
            this.setUniform('1f', 'dt', dt);
            this.bindBuffers();
            gc.beginTransformFeedback(gc.POINTS);
            gc.drawArrays(gc.POINTS, 0, this.nParticles);
            gc.endTransformFeedback();
            this.swapBuffers();
        }
        return ms;
    }
}
