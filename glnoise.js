import { WebGL2SimpleAnimation } from './gltools.js';
import { enableFullScreen, enableOSD } from './animation.js';
import { shaderPair } from './shaders/glnoise-shaders.js';

export class GlNoise extends WebGL2SimpleAnimation {
    constructor(canvas) {
        super(canvas);
        this.shaders.push(...shaderPair);
    }

    init() {
        const { gc, width, height } = super.init();
        gc.viewport(0, 0, width, height);
        gc.clearColor(0, 0, 0, 0);

        const program = this.createProgramFromSources(gc, this.shaders);
        gc.useProgram(program);

        // THIS IS RIDICULOUS VERBOSE
        const vertexArray = [1, 1, 1, -1, -1, 1, -1, 1, 1, -1, -1, -1];
        const vertexPtr = gc.getAttribLocation(program, 'vertexPos');
        const vertexArrBuf = gc.createBuffer();
        gc.bindBuffer(gc.ARRAY_BUFFER, vertexArrBuf);
        gc.bufferData(
            gc.ARRAY_BUFFER,
            new Float32Array(vertexArray),
            gc.STATIC_DRAW
        );
        const vao = gc.createVertexArray();
        gc.bindVertexArray(vao);
        gc.enableVertexAttribArray(vertexPtr);
        gc.vertexAttribPointer(vertexPtr, 2, gc.FLOAT, false, 0, 0);
        // UGHHH

        gc.clear(gc.COLOR_BUFFER_BIT);

        enableFullScreen.call(this);
        enableOSD.call(this, { color: 'yellow' });

        return this;
    }

    nextFrame(ms) {
        if (ms - this.lastRedraw < 50) return this.lastRedraw;
        const { gc } = this;
        this.setUniform('1f', 'time', ms);
        gc.drawArrays(gc.TRIANGLES, 0, 6);
        return ms;
    }
}
