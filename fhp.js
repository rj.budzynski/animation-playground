import { SimpleAnimation, enableOSD } from './animation.js';
const { abs } = Math;
class LatticeGasFHP {
    constructor(width, height) {
        this.latticePair = [
            new Uint8Array(width * height),
            new Uint8Array(width * height)
        ];
        this.width = width;
        this.height = height;
        this.curr = 0;
        this.neighborsOf = this.__proto__.neighborsOf.bind(this);
    }

    neighborsOf(p) {
        const { width: W, height: H } = this;
        const [x, y] = p;
        const out = [];
        const xp = x + 1,
            xm = x - 1,
            yp = y + 1,
            ym = y - 1;
        if (y % 2 === 0) {
            if (x > 0 && yp < H) out.push([xm, yp]);
            //NW -> bit 0
            else out.push(null);
            if (x > 0) out.push([xm, y]);
            //W -> bit 1
            else out.push(null);
            if (x > 0 && y > 0) out.push([xm, ym]);
            //SW -> bit 2
            else out.push(null);
            if (y > 0) out.push([x, ym]);
            //SE -> bit 3
            else out.push(null);
            if (xp < W) out.push([xp, y]);
            //E -> bit 4
            else out.push(null);
            if (yp < H) out.push([x, yp]);
            //NE -> bit 5
            else out.push(null);
            // bit 6 is particle at rest, bit 7 is obstacle
        } else {
            if (yp < H) out.push([x, yp]);
            //NW
            else out.push(null);
            if (x > 0) out.push([xm, y]);
            //W
            else out.push(null);
            if (y > 0) out.push([x, ym]);
            //SW
            else out.push(null);
            if (xp < W && y > 0) out.push([xp, ym]);
            //SE
            else out.push(null);
            if (xp < W) out.push([xp, y]);
            //E
            else out.push(null);
            if (xp < W && yp < H) out.push([xp, yp]);
            //NE
            else out.push(null);
        }
        return out;
    }

    propagateNode(x, y) {
        const { width, neighborsOf } = this;
        const lattice = this.latticePair[this.curr];
        const otherLattice = this.latticePair[+!this.curr];
        const j = x + y * width,
            node = lattice[j];
        const links = neighborsOf([x, y]);

        if (node === 128) return;
        for (let i = 0; i < 6; ++i) {
            const bit = (1 << i) & node;
            let xx, yy;
            if (bit) {
                [xx, yy] = links[i];
                otherLattice[xx + yy * width] |= bit;
                lattice[j] &= ~bit;
            }
        }
    }

    propagate() {
        const width = this.width,
            lattice = this.latticePair[this.curr];
        for (let i = 0, l = lattice.length; i < l; ++i) {
            this.propagateNode(i % width, (i / width) | 0);
        }
        this.curr = +!this.curr;
    }

    collideNode(i) {
        const lattice = this.latticePair[this.curr],
            node = lattice[i];
        // *** FHP-I ***
        // head-on 2-collisions:
        // NW-SE -> 9; W-E -> 18; SW-NE -> 36
        const fhpI2body = [9, 18, 36];
        let idx = fhpI2body.indexOf(node);
        if (idx !== -1) {
            // pick one of the other states "at random"
            lattice[i] = fhpI2body[(idx + (i % 2) + 1) % 3];
            return;
        }
        // symmetric 3-collisions: NW-SW-E or W-SE-NE
        const fhpI3body = [21, 42];
        idx = fhpI3body.indexOf(node);
        if (idx !== -1) {
            lattice[i] = fhpI3body[+!idx];
            return;
        }
        // no at-rest particles in FHP-I
        // obstacle (128) collisions:
        if (node & 128 && node & ~128) {
            let out = 0;
            for (let j = 0; j < 6; ++j) {
                const bit = (node & (1 << j)) >> j;
                if (bit) out |= bit << (j + 3) % 6;
            }
            lattice[i] = out | 128;
        }
    }

    collide() {
        const lattice = this.latticePair[this.curr];
        for (let i = 0, l = lattice.length; i < l; ++i) {
            this.collideNode(i);
        }
    }

    nextState() {
        this.collide();
        this.propagate();
        return this;
    }

    get nParticles() {
        const lattice = this.latticePair[this.curr];
        let n = 0;
        for (let i = 0, l = lattice.length; i < l; ++i) {
            const bits = lattice[i];
            n +=
                (bits & 1) +
                ((bits & 2) >> 1) +
                ((bits & 4) >> 2) +
                ((bits & 8) >> 3) +
                ((bits & 16) >> 4) +
                ((bits & 32) >> 5);
        }
        return n;
    }
}

export class FHPAnimation extends SimpleAnimation {
    init() {
        const { width, height } = super.init();
        this.gas = new LatticeGasFHP(width, height);
        this.frames = 0;

        // reflecting borders **in both lattices**
        for (let i = 0; i < width; ++i) {
            for (const lattice of this.gas.latticePair) {
                const j = (height - 1) * width;
                lattice[i + j] = lattice[i] = 128;
            }
        }
        for (let j = 0; j < height; ++j) {
            for (const lattice of this.gas.latticePair) {
                const i = j * width;
                lattice[i - 1] = lattice[i] = 128;
            }
        }

        // some initial conditions go here
        const lattice = this.gas.latticePair[this.gas.curr];
        const [w, h] = [(width / 2) | 0, (height / 2) | 0];
        for (let y = 1; y < height - 1; ++y) {
            for (let x = 1; x < width - 1; ++x) {
                if ((x - 1.5 * w) ** 2 + (y - 0.5 * h) ** 2 < 0.3 * w * w)
                    lattice[x + y * width] =
                        63 & ~(1 << ((6 * Math.random()) | 0));
                if (abs(x - y) < 10 && abs(x - w) > 15)
                    lattice[x + y * width] = 128;
            }
        }

        enableOSD.call(this, { color: 'gold' });
        this.nParticlesNode = document.createElement('span');
        this.fpsNode.before(this.nParticlesNode);
        this.fpsNode.before(document.createElement('br'));
        this.nParticlesNode.textContent = `${this.gas.nParticles} particles`;

        window.gas = this.gas;
        return this;
    }

    get imageData() {
        return this.gc.getImageData(0, 0, this.width, this.height);
    }

    nextFrame(ms) {
        // for the moment this will be a distorted view (squashed to square lattice)!
        this.gas.nextState();
        const lattice = this.gas.latticePair[this.gas.curr];
        const imageData = this.imageData;
        const data = imageData.data;
        for (let i = 0, l = lattice.length; i < l; ++i) {
            const pix = 4 * i;
            let bits = lattice[i];
            if (bits & 128) {
                continue;
            }
            let nBits = 0;
            for (let k = 0; k < 6; ++k) {
                nBits += (bits >> k) & 1;
            }
            data[pix + 2] = data[pix + 1] = data[pix] = 255;
            data[pix + 3] = nBits * 42;
        }

        this.gc.putImageData(imageData, 0, 0);

        // draw the barrier
        const { width, height } = this;
        const [w, h] = [(width / 2) | 0, (height / 2) | 0];
        this.gc.beginPath();
        this.gc.strokeStyle = 'blue';
        this.gc.lineWidth = 10;
        this.gc.moveTo(0, 0);
        this.gc.lineTo(w - 15, w - 15);
        this.gc.moveTo(w + 15, w + 15);
        this.gc.lineTo(width - 1, width - 1);
        this.gc.stroke();
        return ms;
    }
}
