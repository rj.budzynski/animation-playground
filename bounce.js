import { SimpleAnimation } from './animation.js';
const { random, sin, cos, PI, sign, sqrt } = Math;
const TWO_PI = 2 * PI;

export class BouncingBall extends SimpleAnimation {
    init({ radius = 0.045, accel = 3e-7 } = {}) {
        const { gc, width, height } = super.init();
        gc.setTransform(width, 0, 0, height, 0, 0);
        return Object.assign(this, { radius, accel });
    }

    nextState(ms) {
        const { lastRedraw, radius } = this;
        let { position, velocity } = this;

        if (position === undefined) {
            position = this.position = [0.5, 0.5];
        }
        if (velocity === undefined) {
            const phi = random() * TWO_PI;
            velocity = this.velocity = [cos(phi), sin(phi)].map(v => v * 5e-4);
            // this is only the energy of vertical motion, d.o.f. separate here
            this.energy = 0.5 * velocity[1] ** 2 - this.accel * position[1];
        }
        if (lastRedraw !== undefined) {
            const dt = ms - lastRedraw;
            const nextPos = [...position],
                nextVel = [...velocity];
            for (const i of [0, 1]) {
                nextPos[i] += dt * nextVel[i];
                if (nextPos[i] > 1 - radius) {
                    nextPos[i] = 1 - radius;
                    nextVel[i] *= -1;
                }
                if (nextPos[i] < radius) {
                    nextPos[i] = radius;
                    nextVel[i] *= -1;
                }
            }
            let vsquared = 2 * (this.energy + this.accel * nextPos[1]);
            if (vsquared < 0) {
                vsquared *= -1;
                nextPos[1] = (vsquared * 0.5 - this.energy) / this.accel;
                nextVel[1] *= -1;
            }
            nextVel[1] = sign(nextVel[1]) * sqrt(vsquared);
            this.position = nextPos;
            this.velocity = nextVel;
        }
    }

    nextFrame(ms) {
        // draw the next frame according to elapsed milliseconds
        this.nextState(ms);
        const { gc, position, radius } = this;
        const r = radius * 1.2;
        
        const grad = gc.createRadialGradient(...position, 0, ...position, r);
        grad.addColorStop(0, 'rgba(255,255,128,1)');
        grad.addColorStop(0.9, 'rgba(255,255,128,.3)');
        grad.addColorStop(1, 'rgba(255,255,128,0)');
        gc.fillStyle = 'rgba(0, 0, 0, .4)';
        gc.fillRect(0, 0, 1, 1);
        gc.fillStyle = grad;
        const [x, y] = position;
        gc.fillRect(x - r, y - r, 2 * r, 2 * r);
        gc.fill();

        return ms;
    }
}
