import { SimpleAnimation } from './animation.js';

export class WebGL2SimpleAnimation extends SimpleAnimation {
    constructor(canvas) {
        super(canvas, 'webgl2', { premultipliedAlpha: false });
        if (!this.gc) {
            alert('get a current browser!');
            return;
        }
        this.shaders = [];
    }

    setSize(...size) {
        super.setSize(...size);
        this.gc.viewport(0, 0, this.width, this.height);
        return this;
    }

    createProgramFromSources(
        gc,
        shaderPair,
        macros = {},
        transform_feedback_vars
    ) {
        shaderPair = shaderPair.map(v => {
            for (const [key, val] of Object.entries(macros)) {
                v = v.replace(new RegExp(`\\$${key}`), val);
            }
            return v;
        });
        const shaderType = [gc.VERTEX_SHADER, gc.FRAGMENT_SHADER];
        const shaders = shaderPair.map((shaderSrc, i) => {
            const shader = gc.createShader(shaderType[i]);
            gc.shaderSource(shader, shaderSrc);
            gc.compileShader(shader);
            if (!gc.getShaderParameter(shader, gc.COMPILE_STATUS)) {
                throw 'failed to compile shader: ' +
                    gc.getShaderInfoLog(shader);
            }
            return shader;
        });
        const program = gc.createProgram();
        shaders.forEach(shader => void gc.attachShader(program, shader));
        if (transform_feedback_vars !== undefined) {
            gc.transformFeedbackVaryings(
                program,
                transform_feedback_vars,
                gc.INTERLEAVED_ATTRIBS
            );
        }
        gc.linkProgram(program);
        if (!gc.getProgramParameter(program, gc.LINK_STATUS)) {
            throw 'failed to link program: ' + gc.getProgramInfoLog(program);
        }
        (this.glPrograms = this.glPrograms || []).push(program);
        return (this.program = program);
    }

    setUniform(type, name, ...values) {
        const namePtr = name + 'Ptr';
        this[namePtr] =
            this[namePtr] || this.gc.getUniformLocation(this.program, name);
        this.gc['uniform' + type](this[namePtr], ...values);
    }
}
