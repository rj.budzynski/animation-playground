import { SimpleAnimation } from './animation.js';

const { sin, cos, PI, random, abs, max, min } = Math;
const TWO_PI = 2 * PI;

export class MultiBeads extends SimpleAnimation {
    init(N = 37) {
        this.N = N;
        this.Phi = new Float32Array(N);
        this.Omega = new Float32Array(N);
        this.createOSD();
        return this.setSize();
    }

    createOSD(osdStyle={}) {
        this.osd = document.createElement('p');
        this.osd.appendChild(new Text(`N=${this.N}; avg. velocity: `));
        this.fpsNode = document.createElement('span');
        this.osd.appendChild(this.fpsNode);
        this.osd.appendChild(new Text(' rad/s'))
        Object.assign(
            this.osd.style,
            {
                position: 'absolute',
                left: '1em',
                top: '0',
                fontWeight: 'bold',
                color: 'white'
            },
            osdStyle
        );
        this.gc.canvas.after(this.osd);
        this.nextFrame = ms => {
            const t = this.__proto__.nextFrame.call(this, ms);
            this.updateOSD(ms);
            return t;
        }

        this.updateOSD = ms => {
            if (this.lastUpdate === undefined) {
                this.lastUpdate = ms;
                this.fpsNode.textContent = '0.0';
                return;
            }
            const dt = ms - this.lastUpdate;
            if (dt > 1e3) {
                this.fpsNode.textContent = (this.avgVelocity).toPrecision(3);
                return this.lastUpdate = ms;
            }
        }
    }

    spacing(k) {
        const knext = (k + 1) % this.N;
        return (this.Phi[knext] - this.Phi[k] + TWO_PI) % TWO_PI;
    }

    nextPos(ms) {
        if (this.lastRedraw === undefined) {
            for (let k = this.N; k--; ) {
                this.Phi[k] = (k / this.N) * TWO_PI;
                this.Omega[k] = (1 - random()) * 1e-4;
                this.accel = 1e-7;
                this.timeStarted = ms;
            }
        } else {
            if (ms - this.timeStarted < 1e3) return;
            let dt = ms - this.lastRedraw;
            if (dt > 1e3) dt = 17;
            for (let k = this.N; k--; ) {
                if (this.spacing(k) < TWO_PI / 1.5 / this.N
                    && this.Omega[k] > 0
                ) {
                    this.Omega[k] -= 2 * this.accel * dt;
                } else if (this.Omega[k] < 1e-3) {
                    this.Omega[k] += this.accel * dt;
                }
                if (this.Omega[k] < 0 || this.spacing(k) < TWO_PI / 3 / this.N) {
                    this.Omega[k] = 0;
                }
                this.Phi[k] += dt * this.Omega[k];
                this.Phi[k] %= TWO_PI;
            }
        }
    }

    get avgVelocity() {
        let v = 0;
        for (let k = this.N; k--;) {
            v += this.Omega[k];
        }
        return v / this.N * 1e3;  // radians per second
    }

    nextFrame(ms) {
        // draw the next frame according to elapsed milliseconds
        const { gc, width, height } = this;
        this.nextPos(ms);
        gc.lineWidth = 2;
        gc.clearRect(0, 0, width, height);
        gc.beginPath();
        gc.strokeStyle = 'rgba(0, 255, 255, 1)';
        gc.arc(width / 2, height / 2, (3 * width) / 8, 0, TWO_PI);
        gc.stroke();
        for (let k = this.N; k--; ) {
            gc.beginPath();
            gc.fillStyle =
                k % 2
                    ? 'rgba(0, 255, 255, 1)'
                    : !k
                    ? 'rgba(255, 0, 0, 1)'
                    : 'rgba(255, 255, 0, 1)';
            gc.arc(
                (width / 2) * (1 + 0.75 * cos(this.Phi[k])),
                height / 2 - ((3 * width) / 8) * sin(this.Phi[k]),
                4,
                0,
                TWO_PI
            );
            gc.fill();
        }
        return ms;
    }

}

