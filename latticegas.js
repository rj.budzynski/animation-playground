import { SimpleAnimation, enableOSD } from './animation.js';

class LatticeGasHPP {
    constructor(width, height) {
        this.lattice = new Uint8Array(width * height);
        this.width = width;
        this.height = height;
        this.directions = [this.up, this.left, this.down, this.right].map(f =>
            f.bind(this)
        );
    }

    up(x, y) {
        return ++y < this.height ? [x, y] : null;
    }

    left(x, y) {
        return --x < 0 ? null : [x, y];
    }

    down(x, y) {
        return --y < 0 ? null : [x, y];
    }

    right(x, y) {
        return ++x < this.width ? [x, y] : null;
    }

    propagateNode(x, y) {
        const { lattice, width, directions } = this;
        const node = lattice[x + y * width];
        for (let i = 0; i < 4; ++i) {
            const bit = ((1 << i) & node) >> i;
            if (bit) {
                const [xx, yy] = directions[i](x, y);
                lattice[xx + yy * width] |= bit << (i + 4);
            }
        }
    }

    propagate() {
        const { lattice, width } = this;
        for (let i = 0, l = lattice.length; i < l; ++i) {
            const [x, y] = [i % width, (i / width) | 0];
            this.propagateNode(x, y);
        }
        for (let i = 0, l = lattice.length; i < l; ++i) lattice[i] >>= 4;
    }

    collideNode(x, y) {
        const { lattice, width, height } = this;
        const i = x + y * width;
        const node = lattice[i];
        if (node === 5) lattice[i] = 10;
        else if (node === 10) lattice[i] = 5;
        if (x === 0 || y === 0 || x === width - 1 || y === height - 1)
            lattice[i] = (node * 4) % 15;
    }

    collide() {
        const { lattice, width } = this;
        for (let i = 0, l = lattice.length; i < l; ++i)
            this.collideNode(i % width, (i / width) | 0);
    }

    nextState() {
        this.propagate();
        this.collide();
        return this;
    }

    get nParticles() {
        let n = 0;
        for (let i = 0, l = this.lattice.length; i < l; ++i) {
            const bits = this.lattice[i];
            n +=
                (bits & 1) +
                ((bits & 2) >> 1) +
                ((bits & 4) >> 2) +
                ((bits & 8) >> 3);
        }
        return n;
    }
}

export class HPPAnimation extends SimpleAnimation {
    init() {
        const { width, height } = super.init();
        this.gas = new LatticeGasHPP(width, height);
        const [w, h] = [(width / 2) | 0, (height / 2) | 0];
        const lattice = this.gas.lattice;
        for (let y = 1; y < height - 1; ++y) {
            for (let x = 1; x < width - 1; ++x) {
                // lattice[x + y * width] = 1 << ((4 * Math.random()) | 0);
                if ((x - 0.7 * w) ** 2 + (y - 1.2 * h) ** 2 < 0.5 * w * w)
                    // if (y > x)
                    lattice[x + y * width] =
                        15 ^ (1 << ((4 * Math.random()) | 0));
            }
        }
        enableOSD.call(this, { color: 'gold' });
        this.nParticlesNode = document.createElement('span');
        this.fpsNode.before(this.nParticlesNode);
        this.fpsNode.before(document.createElement('br'));
        this.nParticlesNode.textContent = `${this.gas.nParticles} particles`;
        window.app = this;
        return this;
    }

    get imageData() {
        return this.gc.getImageData(0, 0, this.width, this.height);
    }

    nextFrame(ms) {
        const { gas, gc, imageData } = this;
        const lattice = gas.lattice;
        gas.nextState();
        const data = imageData.data;
        for (let i = 0, l = lattice.length; i < l; ++i) {
            const bits = lattice[i];
            const nBits =
                (bits & 1) +
                ((bits & 2) >> 1) +
                ((bits & 4) >> 2) +
                ((bits & 8) >> 3);
            data[4 * i] = data[4 * i + 1] = data[4 * i + 2] = 255;
            data[4 * i + 3] = nBits * 62;
        }
        gc.putImageData(imageData, 0, 0);

        return ms;
    }
}
