import { WebGL2SimpleAnimation } from './gltools.js';
import { updateShaderPair, renderShaderPair } from './shaders/glball-shaders.js';

const { min, random, sin, cos, PI } = Math;
const TWO_PI = PI * 2;

export class GlBall extends WebGL2SimpleAnimation {
    init({ color = [1, 1, 0.2], accel = 3e-7 } = {}) {
        const { gc, width, height } = super.init();
        this.radius = 0.025;
        this.accel = accel;

        gc.viewport(0, 0, width, height);
        gc.clearColor(0, 0, 0, 0);

        this.updateProgram = this.createProgramFromSources(
            gc,
            updateShaderPair,
            {},
            ['v_position', 'v_velocity']
        );

        this.renderProgram = this.createProgramFromSources(
            gc,
            renderShaderPair
        );

        // the fixed uniforms
        this.program = this.updateProgram;
        this.setUniform('1f', 'radius', this.radius);
        this.setUniform('1f', 'accel', this.accel);
        this.program = this.renderProgram;
        this.color = color;
        this.setUniform(
            '1f',
            'u_radius',
            1.1 * this.radius * min(width, height) // beware this won't work right on a non-square
        );
        gc.clear(gc.COLOR_BUFFER_BIT);
        this.program = this.updateProgram;

        this.readBuffer = gc.createBuffer();
        this.writeBuffer = gc.createBuffer();

        // I have no idea WTF this is for
        // doesn't seem to make a difference whether it's there or not
        // gc.bindTransformFeedback(
        //     gc.TRANSFORM_FEEDBACK,
        //     gc.createTransformFeedback()
        // );

        return this;
    }

    get program() {
        return this._program;
    }

    set program(p) {
        this.gc.useProgram((this._program = p));
    }

    get color() {
        return this._color;
    }

    set color(c) {
        if (this.program === this.renderProgram) {
            this.setUniform('3fv', 'color', c);
        } else {
            this.program = this.renderProgram;
            this.setUniform('3fv', 'color', c);
            this.program = this.updateProgram;
        }
        this._color = c;
    }

    nextState(ms) {
        const { lastRedraw, gc } = this;

        if (lastRedraw === undefined) {
            this.position = [random(), random()].map(
                v => this.radius + v * (1 - 2 * this.radius)
            );
            const phi = random() * TWO_PI;
            this.velocity = [cos(phi), sin(phi)].map(v => v * 5e-4);
            gc.bindBuffer(gc.ARRAY_BUFFER, this.readBuffer);
            gc.bufferData(
                gc.ARRAY_BUFFER,
                new Float32Array([...this.position, ...this.velocity]),
                gc.DYNAMIC_COPY
            );
            gc.bindBuffer(gc.ARRAY_BUFFER, this.writeBuffer);
            gc.bufferData(
                gc.ARRAY_BUFFER,
                new Float32Array([...this.position, ...this.velocity]),
                gc.DYNAMIC_COPY
            );
            gc.bindBuffer(gc.ARRAY_BUFFER, null);
            this.setUniform(
                '1f',
                'yenergy',
                0.5 * this.velocity[1] ** 2 + this.accel * this.position[1]
            );
        } else {
            const dt = ms - lastRedraw;
            this.program = this.updateProgram;
            this.setUniform('1f', 'dt', dt);
            gc.bindBufferBase(
                gc.TRANSFORM_FEEDBACK_BUFFER,
                0,
                this.writeBuffer
            );
            gc.bindBuffer(gc.ARRAY_BUFFER, this.readBuffer);
            // DEBUG
            // {
            //     const buf = new Float32Array(4);
            //     gc.getBufferSubData(gc.ARRAY_BUFFER, 0, buf);
            //     console.log('READ BUFFER CONTENTS:', buf);
            // }
            gc.enable(gc.RASTERIZER_DISCARD);
            gc.enableVertexAttribArray(0);
            gc.enableVertexAttribArray(1);
            gc.vertexAttribPointer(
                0, // this.i_positionPtr,
                2,
                gc.FLOAT,
                false,
                0,
                0
            );
            gc.vertexAttribPointer(
                1, // this.i_velocityPtr,
                2,
                gc.FLOAT,
                false,
                0,
                8
            );
            gc.beginTransformFeedback(gc.POINTS);
            gc.drawArrays(gc.POINTS, 0, 1);
            gc.endTransformFeedback();
            gc.disable(gc.RASTERIZER_DISCARD);
            gc.bindBuffer(gc.ARRAY_BUFFER, null);
            gc.bindBufferBase(gc.TRANSFORM_FEEDBACK_BUFFER, 0, null);
        }
    }

    nextFrame(ms) {
        const { gc } = this;
        this.nextState(ms);
        this.program = this.renderProgram;
        gc.bindBuffer(gc.ARRAY_BUFFER, this.readBuffer);
        gc.enableVertexAttribArray(0);
        gc.vertexAttribPointer(0, 2, gc.FLOAT, false, 0, 0);
        gc.clear(gc.COLOR_BUFFER_BIT);
        gc.drawArrays(gc.POINTS, 0, 1);
        gc.bindBuffer(gc.ARRAY_BUFFER, null);
        [this.readBuffer, this.writeBuffer] = [
            this.writeBuffer,
            this.readBuffer
        ];
        return ms;
    }
}
