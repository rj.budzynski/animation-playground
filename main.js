import { AnalogClock } from './clock.js';
import { BouncingBall } from './bounce.js';
import { SimpleAnimation } from './animation.js';
import { Videonoise } from './noise.js';
import { GlTextureAnimation } from './gltexture.js';
import { GlBall } from './glball.js';
import { GlNoise } from './glnoise.js';
import { GlWaves } from './glwaves.js';
import { HPPAnimation } from './latticegas.js';
import { FHPAnimation } from './fhp.js';
import { GlMultiparticle } from './multiparticle.js';
import { MultiBeads } from './multibeads.js';

const modes = [
    SimpleAnimation,
    AnalogClock,
    BouncingBall,
    Videonoise,
    GlTextureAnimation,
    GlBall,
    GlNoise,
    GlWaves,
    HPPAnimation,
    FHPAnimation,
    GlMultiparticle,
    MultiBeads
];

const main = () => {
    const canvas = document.querySelector('canvas');
    const modeIdx = document.location.hash.substring(1) || '0';
    console.log(modeIdx);
    const app = new modes[modeIdx](canvas).init();
    app.start();

    const toc = document.querySelector('#toc');
    for (const el of toc.querySelectorAll('li')) {
        el.addEventListener('click', ev => {
            ev.preventDefault();
            document.location.hash = '#' + el.id;
            document.location.reload();
        });
        if (el.id === modeIdx) {
            el.className = 'current';
        }
    }

    canvas.addEventListener('click', ev => {
        if (app.running) app.pause();
        else app.resume();
        ev.stopPropagation();
    });
};

window.addEventListener('load', main);
