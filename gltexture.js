import { WebGL2SimpleAnimation } from './gltools.js';
import {
    textureShaderPair,
    renderShaderPair
} from './shaders/gltexture-shaders.js';
import { enableFullScreen, enableOSD } from './animation.js';
const { sin, cos, PI } = Math;

export class GlTextureAnimation extends WebGL2SimpleAnimation {
    init(nWaves = 5) {
        const { gc, width, height } = this.setSize();
        gc.canvas.onresize = () => this.onResize();
        gc.canvas.parentElement.style.background = 'rgb(224,92,224)';
        gc.canvas.style.border = '0';
        gc.clearColor(0, 0, 0, 0);

        const texture = gc.createTexture();
        gc.activeTexture(gc.TEXTURE0);
        gc.bindTexture(gc.TEXTURE_2D, texture);

        gc.texParameteri(gc.TEXTURE_2D, gc.TEXTURE_MIN_FILTER, gc.NEAREST);
        gc.texParameteri(gc.TEXTURE_2D, gc.TEXTURE_MAG_FILTER, gc.NEAREST);

        gc.texImage2D(
            gc.TEXTURE_2D,
            0,
            gc.RGBA,
            width,
            1, // texture height
            0,
            gc.RGBA,
            gc.UNSIGNED_BYTE,
            null
        );

        const framebuffer = gc.createFramebuffer();
        gc.bindFramebuffer(gc.FRAMEBUFFER, framebuffer);
        gc.framebufferTexture2D(
            gc.FRAMEBUFFER,
            gc.COLOR_ATTACHMENT0,
            gc.TEXTURE_2D,
            texture,
            0
        );

        const program = this.createProgramFromSources(gc, textureShaderPair);
        gc.useProgram(program);

        gc.viewport(0, 0, width, 1);
        gc.clear(gc.COLOR_BUFFER_BIT);
        gc.drawArrays(gc.TRIANGLES, 0, 6);

        gc.bindFramebuffer(gc.FRAMEBUFFER, null);

        const program2 = this.createProgramFromSources(gc, renderShaderPair, {
            nWaves
        });
        gc.useProgram(program2);
        gc.viewport(0, 0, width, height);
        gc.clearColor(0, 0, 0, 1);
        gc.clear(gc.COLOR_BUFFER_BIT);

        let waveV = [];
        for (let i = 0; i < nWaves; ++i) {
            waveV.push(cos((i * PI) / nWaves), sin((i * PI) / nWaves));
        }

        this.setUniform('2fv', 'waveV', waveV);
        this.setUniform('1i', 'image', 0);
        this.setUniform('2f', 'resolution', width, height);

        gc.viewport(0, 0, width, height);

        enableFullScreen.call(this);
        enableOSD.call(this);

        return this;
    }

    get time() {
        return this._time;
    }

    set time(t) {
        this.setUniform('1f', 'time', t);
        this._time = t;
    }

    nextFrame(ms) {
        const { gc, lastRedraw } = this;
        if (lastRedraw === undefined) {
            this.time = 0;
        } else {
            const dt = ms - lastRedraw;
            this.time += dt;
        }
        gc.drawArrays(gc.TRIANGLES, 0, 6);
        return ms;
    }
}
