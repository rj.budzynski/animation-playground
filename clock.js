import { SimpleAnimation } from './animation.js';
const { min, sin, cos, PI } = Math;
const TWO_PI = 2 * PI;

export class AnalogClock extends SimpleAnimation {
    init() {
        const canvas = this.gc.canvas,
            gc = this.gc;
        // setup geometry
        const width = (canvas.width = canvas.clientWidth);
        const height = (canvas.height = canvas.clientHeight);
        const size = min(width, height);
        canvas.style.borderRadius = `${(height / 2) | 0}px`;
        if (canvas.parentElement)
            canvas.parentElement.style.backgroundColor = 'black';
        gc.setTransform(size * 0.45, 0, 0, -size * 0.45, width / 2, height / 2);
        gc.fillStyle = 'rgba(0, 0, 0, 1)';
        gc.lineCap = 'round';
        return this;
    }

    nextFrame(ms) {
        // a very simple one for testing purposes
        if (ms - this.lastRedraw < 1e3) return this.lastRedraw;
        const gc = this.gc,
            now = new Date();
        let phi;
        gc.clearRect(-1.2, -1.2, 2.4, 2.4);
        gc.beginPath();
        gc.lineWidth = 0.04;
        gc.strokeStyle = 'red';
        gc.moveTo(0, 0);
        phi = (TWO_PI * ((now.getHours() % 12) + now.getMinutes() / 60)) / 12;
        gc.lineTo(0.66 * sin(phi), 0.66 * cos(phi));
        gc.stroke();
        gc.beginPath();
        gc.lineWidth = 0.03;
        gc.strokeStyle = 'green';
        gc.moveTo(0, 0);
        phi = (TWO_PI * now.getMinutes()) / 60;
        gc.lineTo(sin(phi), cos(phi));
        gc.stroke();
        gc.beginPath();
        gc.lineWidth = 0.005;
        gc.strokeStyle = 'yellow';
        gc.moveTo(0, 0);
        phi = (TWO_PI * now.getSeconds()) / 60;
        gc.lineTo(sin(phi), cos(phi));
        gc.stroke();
        gc.beginPath();
        gc.arc(0, 0, 0.07, 0, TWO_PI);
        gc.fill();
        return ms;
    }
}
