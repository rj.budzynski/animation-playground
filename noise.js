import { SimpleAnimation, enableFullScreen, enableOSD } from './animation.js';
const { random } = Math;

export class Videonoise extends SimpleAnimation {
    init() {
        const { gc } = super.init();
        gc.canvas.parentElement.style.background = 'white';
        enableFullScreen.call(this);
        enableOSD.call(this);
        return this;
    }

    get npixels() {
        return this.width * this.height;
    }

    get imageData() {
        return this.gc.getImageData(0, 0, this.width, this.height);
    }

    nextFrame(ms) {
        const { gc, imageData, npixels } = this;
        const data = imageData.data;
        for (let i = 0; i < npixels; ++i) {
            data[i * 4 + 3] = (random() * 256) | 0;
        }
        gc.putImageData(imageData, 0, 0);
        return ms;
    }
}
