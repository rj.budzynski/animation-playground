export const shaderPair = [
    `#version 300 es

uniform float dt;
uniform float radius;
uniform float accel;
uniform float u_radius;

layout(location=0) in vec2 i_position;
layout(location=1) in vec2 i_velocity;
layout(location=2) in float i_yenergy;

out vec2 v_position;
out vec2 v_velocity;
out float v_yenergy;

void main() {
    v_yenergy = i_yenergy;
    v_velocity = i_velocity; 
    vec2 nextPos = i_position + dt * i_velocity;
    for (int i = 0; i < 2; ++i) {
        if (nextPos[i] < radius) {
            nextPos[i] = radius;
            v_velocity[i] *= -1.;
        } else if (nextPos[i] > 1. - radius) {
            nextPos[i] = 1. - radius;
            v_velocity[i] *= -1.; 
        }
    }
    float vsquared = 2. * (i_yenergy - accel * nextPos[1]);
    if (vsquared < 0.) {
        vsquared *= -1.;
        nextPos[1] = (i_yenergy - .5 * vsquared) / accel;
        v_velocity[1] *= -1.;
    }
    v_velocity[1] = sign(v_velocity[1]) * sqrt(vsquared);
    v_position = nextPos;
    gl_Position = vec4(2. * v_position - 1., 0., 1.);
    gl_PointSize = 2. * u_radius;
}`,

    `#version 300 es

precision mediump float;

uniform vec3 color;
out vec4 fragColor;

void main() {
    float d = 1. - distance(gl_PointCoord, vec2(.5));
    float alpha = step(0.5, d) * clamp(d, 0., 1.);
    fragColor = vec4(color, alpha);
}`
];
