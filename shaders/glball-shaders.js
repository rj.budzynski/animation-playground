// -------- glball's shader pair: the render stage shader

export const renderShaderPair = [
    `#version 300 es

layout(location=0) in vec2 position;
uniform float u_radius;
out vec2 v_position;

void main() {
    v_position = 2. * position - 1.;
    gl_Position = vec4(v_position, 0., 1.);
    gl_PointSize = 2. * u_radius;
}
`,
    `#version 300 es

precision mediump float;

in vec2 v_position;
uniform vec3 color;
out vec4 fragColor;

void main() {

    float d = 1. - distance(gl_PointCoord, vec2(.5));
    float alpha = step(0.5, d) * clamp(d, 0., 1.);
    fragColor = vec4(color, alpha);
}
`
];

// ------- better glball's update program shader pair

export const updateShaderPair = [
    `#version 300 es

uniform float dt;
uniform float radius;
uniform float accel;
uniform float yenergy;

layout(location=0) in vec2 i_position;
layout(location=1) in vec2 i_velocity;
out vec2 v_position;
out vec2 v_velocity;

void main() {
    vec2 nextPos;
    v_velocity = i_velocity; //
    nextPos = i_position + dt * i_velocity;
    for (int i = 0; i < 2; ++i) {
        if (nextPos[i] < radius) {
            nextPos[i] = radius;
            v_velocity[i] *= -1.;
        } else if (nextPos[i] > 1. - radius) {
            nextPos[i] = 1. - radius;
            v_velocity[i] *= -1.; 
        }
    }
    
    float vsquared = 2. * (yenergy - accel * nextPos[1]);
    if (vsquared < 0.) {
        vsquared *= -1.;
        nextPos[1] = (yenergy - .5 * vsquared) / accel;
        v_velocity[1] *= -1.;
    }
    v_velocity[1] = sign(v_velocity[1]) * sqrt(vsquared);
    v_position = nextPos;
}
`,

    // ----- a no-op fshader

    `#version 300 es

precision mediump float;

void main() { discard; }
`
];
