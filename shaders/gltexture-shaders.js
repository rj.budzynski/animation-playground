// save precomputed sine function values to a texture

export const textureShaderPair = [
    `#version 300 es

out float u;

void main() {
    vec2 posArr[] = vec2[6](
        vec2(1.,1.), vec2(1., -1.), vec2(-1., 1.),
        vec2(-1., 1.), vec2(1., -1), vec2(-1., -1.)
        );
    gl_Position = vec4(posArr[gl_VertexID], 0., 1.);
    u = gl_Position.x + 1.; 
}
`,

    `#version 300 es
    
precision mediump float;
#define PI radians(180.)
in float u;
out vec4 fragColor;

void main() {
    float amplitude;
    amplitude = sin(u*PI);
    fragColor = vec4(0., 0., 0., .5 * (amplitude + 1.));
}
`
];

export const renderShaderPair = [
    `#version 300 es
    
void main() {
    vec2 posArr[] = vec2[6](
        vec2(1.,1.), vec2(1., -1.), vec2(-1., 1.),
        vec2(-1., 1.), vec2(1., -1), vec2(-1., -1.)
        );
    gl_Position = vec4(posArr[gl_VertexID], 0., 1.);
}
`,

    `#version 300 es

#define VELOCITY 1.3e-4
#define nWaves $nWaves

precision mediump float;
uniform vec2 waveV[nWaves];
uniform vec2 resolution;
uniform sampler2D image;
uniform float time;
out vec4 fragColor;

float myfilter(in float amplitude) {
    return mod(floor(amplitude), 2.) == 0. ? 
                    fract(amplitude) : 
                    fract(1. - amplitude);
}

void main() {
    float amplitude = 0.;
    for (int i = 0; i < nWaves; ++i) {
        vec2 uv = gl_FragCoord.xy / min(resolution.x, resolution.y);
        float u = dot(uv, waveV[i]);
        vec2 tex_position = vec2(13.*u - VELOCITY * time, 0.);
        amplitude += texture(image, tex_position).a;
    }
    fragColor = vec4(0., 0., 0., myfilter(2.*amplitude));
}
`
];
