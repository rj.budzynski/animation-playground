export const shaderPair = [
    `#version 300 es

in vec4 vertexPos;

void main() {
    gl_Position = vertexPos;
}
`,

    `#version 300 es

precision mediump float;

#define PI radians(180.)
#define nWaves 7.
uniform vec2 resolution;
uniform float time;  // in milliseconds
out vec4 fragColor;
float angle = 2. * PI/nWaves;

float rand(in float x) {
    float a = 12.9898;
    float b = 78.233;
    float c = 43758.5453;
    float phi= mod(a*x+b, PI);
    return fract(sin(c*phi));
}

float myfilter(in float amplitude) {
    return mod(floor(amplitude), 2.) == 0. ? 
                    fract(amplitude) : 
                    fract(1. - amplitude);
}

void main() {
    float alpha = 0.;
    float t = time * 5e-4;
    vec2 uv = gl_FragCoord.xy / resolution;
        
    for (float k=0.; k <nWaves; ++k) {
        float x = uv.x * cos(k*angle) - uv.y * sin(k*angle);
        x *= 25.;
        x -= t;
        // t *= -1.;
        float i = floor(x);
        float f = fract(x);
        alpha += mix(rand(i), rand(i+1.), smoothstep(0., 1., f));            
    }
    alpha = myfilter(alpha);
    fragColor = vec4(vec3(0.), alpha);
}
`
];
