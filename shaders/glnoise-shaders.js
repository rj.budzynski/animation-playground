export const shaderPair = [
    `#version 300 es

in vec4 vertexPos;

void main() {
    gl_Position = vertexPos;
}
`,

    `#version 300 es

precision mediump float;
#define PI radians(180.)
uniform float time;  // in milliseconds
out vec4 fragColor;

/***
 * Based on https://arxiv.org/pdf/2004.06278v3.pdf
 * 
 ***/

uint hash(uint x, uint ctr) {
    uint y;
    y = x = ctr * x;
    uint z;
    z = y + ctr;
    x = x * x + y;
    x = (x >> 16) | (x << 16);
    x = x * x + z;
    x = (x >> 16) | (x << 16);
    x = x * x + y;
    x = (x >> 16) | (x << 16);
    return (x * x + z) >> 9;
}

float random(float seed, uint ctr) {
    uint floatOne = uint(0x3F800000);
    uint x = floatBitsToUint(seed);
    uint h = hash(x, ctr) | floatOne;
    float r = uintBitsToFloat(h);
    return r - 1.;
}
void main() {
    uint ctr = uint(1<<12) * uint(floor(gl_FragCoord.y)) + uint(floor(gl_FragCoord.x));
    fragColor = vec4(1., 1., 1., random(time, ctr));
}
`,
];
